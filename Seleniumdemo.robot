*** Settings ***
Suite Setup        Suite Setup
Test Setup         Test Setup
Test Teardown      Test Teardown
Suite Teardown     Suite Teardown
Library            SeleniumLibrary
Resource           Labels.robot
Documentation      Test suite to demonstrate simple web site testing using Seleniun library.
    ...            See https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html for available keywords.
Force Tags         Selenium    Demo


*** Test Cases ***
Verify Elements
    [Documentation]                   Check that our test page contains the basic html elements.
    [Tags]                            Element
    Page Should Contain Element       //h1[@id="pagetitle"]
    Page Should Contain Element       //div[@id="infotext"]
    Page Should Contain Element       //ul[@id="unsorted_list1"]
    Page Should Contain Element       //h2[@id="elementstitle"]

    Page Should Contain Element       //h3[@id="radiobuttonstitle"]
    Page Should Contain Radio Button  id=first_button
    Page Should Contain Radio Button  id=second_button
    Page Should Contain Radio Button  id=third_button
    Page Should Contain Button        id=radiobutton_submit

    Page Should Contain Element       //h3[@id="checkboxestitle"]
    Page Should Contain Checkbox      id=first_box
    Page Should Contain Checkbox      id=second_box
    Page Should Contain Checkbox      id=third_box
    Page Should Contain Button        id=checkbox_submit

    Page Should Contain Element       //h3[@id="textfieldstitle"]
    Page Should Contain Element       //input[@id="textfield"]
    Page Should Contain Button        id=cleartext
    Page Should Contain Element       //textarea[@id="textarea"]
    Page Should Contain Button        id=togglecase_submit

    Page Should Contain Element       //h3[@id="hiddenelementstitle"]
    Page Should Contain Element       //input[@id="hiddenfield1"]
    Page Should Contain Button        id=hidden_submit

    Page Should Contain Element       //input[@id="hiddenfield2"]
    Page Should Contain Button        id=hidden_submit2

    Page Should Contain Element       //h3[@id="alerttitle"]
    Page Should Contain Button        id=alert_button

    Page Should Contain Element       //h3[@id="mutatedelementstitle"]
    Page Should Contain Element       //div[@id="mutablebox"]
    Page Should Contain Button        //button[@id="mutate_submit"]

    Page Should Contain Element       //div[@id="footertext"]

Verify Labels
    [Documentation]                   Check that our test page contains the basic text labels.
    [Tags]                            Label
    Title Should Be                   ${SITETITLE}
    Element Text Should Be            id=pagetitle              ${PAGETITLE}
    Element Text Should Be            id=elementstitle          ${ELEMENTSTITLE}

    Element Text Should Be            id=radiobuttonstitle      ${RADIOBUTTONSTITLE}
    # For elements that use labels we use a different keyword to search for element of type <label> and contains some text
    Element Should Be Visible         //label[text() = '${FIRST_BUTTON}']
    Element Should Be Visible         //label[text() = '${SECOND_BUTTON}']
    Element Should Be Visible         //label[text() = '${THIRD_BUTTON}']
    Element Text Should Be            id=radiobutton_submit     ${RADIOBUTTON_SUBMIT}

    Element Text Should Be            id=checkboxestitle        ${CHECKBOXESTITLE}
    Element Should Be Visible         //label[text() = '${FIRST_BOX}']
    Element Should Be Visible         //label[text() = '${SECOND_BOX}']
    Element Should Be Visible         //label[text() = '${THIRD_BOX}']
    Element Text Should Be            id=checkbox_submit        ${CHECKBOX_SUBMIT}

    Element Text Should Be            id=textfieldstitle        ${TEXTFIELDSTITLE}
    Element Should Be Visible         //label[text() = '${TEXTFIELD}']
    Element Text Should Be            id=cleartext              ${CLEARTEXT_BUTTON}
    Element Text Should Be            id=togglecase_submit      ${TOGGLECASE_SUBMIT}

    Element Text Should Be            id=hiddenelementstitle    ${HIDDENELEMENTSTITLE}
    Text Field Value Should Be        id=hiddenfield1           ${HIDDENFIELD1}
    Element Text Should Be            id=hidden_submit          ${HIDDEN_SUBMIT}
    Text Field Value Should Be        id=hiddenfield2           ${HIDDENFIELD2}
    Element Text Should Be            id=hidden_submit2         ${HIDDEN_SUBMIT2}

    Element Text Should Be            id=alerttitle             ${ALERTTITLE}
    Element Text Should Be            id=alert_button           ${ALERT_BUTTON}

    Element Text Should Be            id=mutatedelementstitle   ${MUTATEDELEMENTSTITLE}
    Element Text Should Be            id=mutate_submit          ${MUTATE_SUBMIT}

Verify Radiobuttons
    [Documentation]                   Check that our radio buttons work as intended.
    [Tags]                            Radiobutton
    Click Button                      id=radiobutton_submit
    Alert Should Be Present           text=No radio button selected!

    Select Radiobutton                group_name=button_demo    value=EKA
    Select Radiobutton                group_name=button_demo    value=TOKA
    Select Radiobutton                group_name=button_demo    value=KOLMAS
    Click Button                      id=radiobutton_submit
    Alert Should Be Present           text=Selected radio button: KOLMAS

Verify Checkboxes
    [Documentation]                   Check that our checkboxes work as intended.
    [Tags]                            Checkbox
    Checkbox Should Be Selected       id=first_box
    Select Checkbox                   id=second_box
    Select Checkbox                   id=third_box
    Click Button                      id=checkbox_submit
    Alert Should Be Present           text=Selected checkboxes: ABBA,ACDC,PMMP

    Unselect Checkbox                 id=first_box
    Unselect Checkbox                 id=second_box
    Unselect Checkbox                 id=third_box
    Click Button                      id=checkbox_submit
    Alert Should Be Present           text=No checkboxes selected!

Verify Text Fields
    [Documentation]                   Check that our text elements work as intended.
    [Tags]                            Textfield
    ${TEST_STRING}                    Set Variable    Lorem Ipsum Blah Blah
    Input Text                        id=textfield    text=${TEST_STRING}
    Textfield Value Should Be         id=textfield    expected=${TEST_STRING}
    Click Button                      id=cleartext
    Textfield Value Should Be         id=textfield    expected=

    ${textarea_content}               Get Text    id=textarea
    Click Button                      id=togglecase_submit
    Element Text Should Be            id=textarea    expected=${textarea_content.upper()}

Verify Hidden
    [Documentation]                   Check that our hidden elements work as intended.
    [Tags]                            Hidden
    Element Should Be Visible         id=hiddenfield1
    Click Element                     id=hidden_submit
    Element Should Not Be Visible     id=hiddenfield1

    Element Should Not Be Visible     id=hiddenfield2
    Click Element                     id=hidden_submit2
    Element Should Be Visible         id=hiddenfield2

Verify Mutation
    [Documentation]                   Check that our mutable elements work as intended.
    [Tags]                            Mutate
    ${colors}                         Create List    blue    red    yellow    coral
    FOR    ${color}    IN    @{colors}
        Capture Element Screenshot        id=mutablebox
        ${css_style}                      Get Element Attribute    id=mutablebox    style
        Should Be Equal                   ${css_style}    background-color: ${color};
        Click Button                      id=mutate_submit
    END


*** Keywords ***
Suite Setup
    ${PAGE_URL}    Set Variable    ${CURDIR}${/}seleniumdemo.htm
    Open Demo Page    ${PAGE_URL}

Test Setup
    Reload Page

Test Teardown
    Run Keyword If Test Failed    Capture Page Screenshot

Suite Teardown
    Close Browser

Open Demo Page
    [Arguments]    ${url}
    Open Browser    url=${url}    browser=Chrome
    Wait Until Page Contains Element   id=pagetitle    timeout=10s
    Capture Page Screenshot
