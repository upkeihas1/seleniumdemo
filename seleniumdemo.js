/* seleniumdemo.js

This javascript file contains the small scripts required to mutate html elements for testing.
*/


function radioButtons() {
  // find all radio button elements by name and check which one is selected
  radiobuttons = document.getElementsByName('button_demo');
  for (let i = 0; i < radiobuttons.length; i++){
    if (radiobuttons[i].checked){
      alert('Selected radio button: ' + radiobuttons[i].value);
      // exit function after displaying alert to suppress default message below
      return;
    }
  }
  alert('No radio button selected!');
}

function checkBoxes() {
  // same same, but multiple checkboxes can be selected at the same time
  checkboxes = document.getElementsByName('box_demo');
  let selected_boxes = [];
  for (let i = 0; i < checkboxes.length; i++){
    let checkbox = checkboxes[i]
    if (checkbox.checked){
      selected_boxes.push(checkbox.value)
    }
  }
  // length 0 == false. javascript is awesome!
  if (selected_boxes.length){
    alert('Selected checkboxes: ' + selected_boxes)
  } else {
    alert('No checkboxes selected!')
  }
}

function clearTextfield() {
  // find text element by id and clear
  document.getElementById('textfield').value = "";
}

function toggleCase(){
  textarea = document.getElementById('textarea')
  if (textarea.style.textTransform === "uppercase") {
    textarea.style.textTransform = "initial"
  } else {
    textarea.style.textTransform = "uppercase"
  }
}

function toggleHidden(field_name) {
  textfield = document.getElementById(field_name)
  if (textfield.style.display === "none") {
    textfield.style.display = "inline-block";
  } else {
    textfield.style.display = "none";
  }
}

function changeColor(element_id) {
  let colors = ['blue', 'red', 'yellow', 'coral'];
  element = document.getElementById(element_id);
  current_color = colors.indexOf(element.style.backgroundColor);
  // if current color is last in list, new value is 0, otherwise old+1
  color = (current_color == 3) ? 0 : current_color + 1;
  element.style.backgroundColor = colors[color];
}

console.info('Loaded seleniumdemo.js')
