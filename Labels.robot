*** Variables ***
${SITETITLE}             Selenium demo page
${PAGETITLE}             Selenium demo

${ELEMENTSTITLE}         Elements
${RADIOBUTTONSTITLE}     Radio buttons
${FIRST_BUTTON}          EKA
${SECOND_BUTTON}         TOKA
${THIRD_BUTTON}          KOLMAS
${RADIOBUTTON_SUBMIT}    Submit

${CHECKBOXESTITLE}       Checkboxes
${FIRST_BOX}             ABBA
${SECOND_BOX}            ACDC
${THIRD_BOX}             PMMP
${CHECKBOX_SUBMIT}       Submit

${TEXTFIELDSTITLE}       Text fields
${TEXTFIELD}             Input text here
${CLEARTEXT_BUTTON}      Clear field
${TOGGLECASE_SUBMIT}     Toggle text case

${HIDDENELEMENTSTITLE}   Hidden elements
${HIDDENFIELD1}          Hide me!
${HIDDEN_SUBMIT}         Hide text field!
${HIDDENFIELD2}          Show me!
${HIDDEN_SUBMIT2}        Show text field!

${ALERTTITLE}            Alert!
${ALERT_BUTTON}          ALERT!

${MUTATEDELEMENTSTITLE}  Mutated elements
${MUTATE_SUBMIT}         Change box color!