# Seleniumdemo

A simple project to demonstrate how web testing using robot framework and selenium library works.


### Requirements
* [Python (64-bit)](https://www.python.org/downloads/)
* [Chromedriver](https://chromedriver.chromium.org/downloads) (Remember to add to PATH)


### Usage
* Clone this repo
* (Following commands in powershell) Upgrade pip: `pip install --upgrade pip`
* Install requirements: `pip install -r requirements.txt`
* Run robot tests: `robot Seleniumdemo.robot`
